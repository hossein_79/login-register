import { Injectable } from '@nestjs/common';
import { v4 as uuid } from 'uuid';
import { User } from './interface/user.interface';

@Injectable()
export class UserRepository {
  users: User[] = [];

  getUserById(id: string) {
    const user = this.users.find(
      (item) => item.id === id && item.isActive === true,
    );

    return user != null ? { ...user } : undefined;
  }

  getUserByEmail(email: string) {
    const user = this.users.find(
      (item) => item.email === email && item.isActive === true,
    );

    return user != null ? { ...user } : undefined;
  }

  insertUser(name: string, email: string, password: string) {
    const now = new Date();

    const user: User = {
      id: uuid(),
      name,
      email,
      password,
      passwordUpdateTime: null,
      isActive: true,
      status: null,
      createdAt: now,
      updatedAt: now,
    };

    this.users.push(user);

    return { ...user };
  }
}
