export interface User {
  id: string;
  name: string;
  email: string;
  password: string;
  passwordUpdateTime: Date;
  isActive: boolean;
  status: number;
  createdAt: Date;
  updatedAt: Date;
}
