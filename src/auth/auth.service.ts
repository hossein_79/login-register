import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserRepository } from 'src/data/user.repository';

interface JwtPayload {
  userId: string;
  createdAt: number;
}

@Injectable()
export class AuthService {
  constructor(
    private userRepository: UserRepository,
    private jwtService: JwtService,
  ) {}

  generateJwtToken(payload: JwtPayload) {
    const accessToken = this.jwtService.sign(payload);

    return accessToken;
  }

  async validateJwt(payload: JwtPayload) {
    const { userId, createdAt } = payload;

    // search for user in database
    const user = await this.userRepository.getUserById(userId);

    // return error if user is not exists
    if (!user) {
      throw new UnauthorizedException();
    }

    if (
      user.passwordUpdateTime != null &&
      user.passwordUpdateTime.getTime() > createdAt
    ) {
      throw new UnauthorizedException();
    }

    return user;
  }
}
