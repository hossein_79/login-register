import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { GetUser } from 'src/auth/decorator/get-user.decorator';
import { JwtAuthGuard } from 'src/auth/guard/jwt-auth.guard';
import { User } from 'src/data/interface/user.interface';
import { UserSignInDto } from './dto/user-sign-in.dto';
import { UserSignUpDto } from './dto/user-sign-up.dto';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Get()
  @UseGuards(JwtAuthGuard)
  getUser(@GetUser() user: User) {
    return this.userService.getUser(user);
  }

  @Post('sign-up')
  signUp(@Body() userSignUpDto: UserSignUpDto) {
    return this.userService.signUp(userSignUpDto);
  }

  @Post('sign-in')
  signIn(@Body() userSignInDto: UserSignInDto) {
    return this.userService.signIn(userSignInDto);
  }
}
