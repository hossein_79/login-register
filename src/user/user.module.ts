import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { AuthModule } from 'src/auth/auth.module';
import { DataModule } from 'src/data/data.module';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
  imports: [HttpModule, DataModule, AuthModule],
  controllers: [UserController],
  providers: [UserService],
})
export class UserModule {}
