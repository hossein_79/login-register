import { HttpService } from '@nestjs/axios';
import { BadRequestException, Injectable } from '@nestjs/common';
import { compare, hash } from 'bcrypt';
import { lastValueFrom } from 'rxjs';
import { AuthService } from 'src/auth/auth.service';
import { User } from 'src/data/interface/user.interface';
import { UserRepository } from 'src/data/user.repository';
import { UserSignInDto } from './dto/user-sign-in.dto';
import { UserSignUpDto } from './dto/user-sign-up.dto';

@Injectable()
export class UserService {
  constructor(
    private userRepository: UserRepository,
    private authService: AuthService,
    private httpService: HttpService,
  ) {}

  async getUser(user: User) {
    delete user.password;

    return { ok: true, result: { user } };
  }

  async signUp(userSignUpDto: UserSignUpDto) {
    const { name, email, password } = userSignUpDto;

    // return error if email is exists
    if (await this.userRepository.getUserByEmail(email)) {
      throw new BadRequestException('User is already exists');
    }

    // hash password
    const hashedPassword = await hash(password, 10);

    // insert user
    const user = await this.userRepository.insertUser(
      name,
      email,
      hashedPassword,
    );

    // generate jwt accessToken
    const accessToken = this.authService.generateJwtToken({
      userId: user.id,
      createdAt: Date.now(),
    });

    const result = {
      user: { ...user, password: undefined },
      accessToken,
    };

    return {
      ok: true,
      result,
    };
  }

  async signIn(userSignInDto: UserSignInDto) {
    const { email, password } = userSignInDto;

    // search for user according to email
    const user = await this.userRepository.getUserByEmail(email);

    // return error if user is not exists
    if (!user) {
      throw new BadRequestException('Invalid credentials');
    }

    // check password
    if (!(await compare(password, user.password))) {
      throw new BadRequestException('Invalid credentials');
    }

    // generate jwt accessToken
    const accessToken = this.authService.generateJwtToken({
      userId: user.id,
      createdAt: Date.now(),
    });

    // call nobitex api
    const nobitexReqParams = {
      srcCurrency:
        'btc,eth,etc,usdt,ada,bch,ltc,bnb,eos,xlm,xrp,trx,doge,uni,link,dai,dot,shib,aave,ftm,matic,axs,mana,sand,pmn',
      dstCurrency: 'rls,usdt',
    };

    const nobitexRes = await lastValueFrom(
      this.httpService.get('https://api.nobitex.ir/market/stats', {
        params: nobitexReqParams,
      }),
    );

    const result = {
      user: { ...user, password: undefined },
      accessToken,
      stats: nobitexRes.data?.stats,
    };

    return {
      ok: true,
      result,
    };
  }
}
