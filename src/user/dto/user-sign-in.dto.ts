import { IsDefined, IsEmail, Matches } from 'class-validator';

export class UserSignInDto {
  @IsDefined()
  @IsEmail()
  email: string;

  @IsDefined()
  @Matches(/^(?!.* )(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/)
  password: string;
}
