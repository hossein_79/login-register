import { Transform } from 'class-transformer';
import {
  IsDefined,
  IsEmail,
  IsNotEmpty,
  IsString,
  Matches,
} from 'class-validator';

export class UserSignUpDto {
  @Transform((i) => (typeof i.value === 'string' ? i.value.trim() : i.value))
  @IsDefined()
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsDefined()
  @IsEmail()
  email: string;

  @IsDefined()
  @Matches(/^(?!.* )(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/)
  password: string;
}
